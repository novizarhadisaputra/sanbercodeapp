import React from 'react';
import { Dimensions, StyleSheet, TextInput, View } from 'react-native';
import PropTypes from 'prop-types';

const InputCustom = (props) => {
	let ScreenHeight = Dimensions.get('window').height;
	let ScreenWidth = Dimensions.get('window').width;

	const styles = StyleSheet.create({
		parent: {
			borderWidth: 1,
			borderColor: 'black',
			paddingHorizontal: ScreenWidth * 0.03,
			paddingVertical: ScreenWidth * 0.01
		}
	});
	return (
		<View style={styles.parent}>
			<TextInput {...props} />
		</View>
	);
};

InputCustom.propTypes = {
	placeholder: PropTypes.string,
	defaultValue: PropTypes.string,
	onChangeText: PropTypes.func
};

export default InputCustom;
