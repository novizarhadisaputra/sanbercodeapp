import React from 'react';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';

const ListTaskThree = ({ value, removeTask }) => {
	let ScreenWidth = Dimensions.get('window').width;
	const styles = StyleSheet.create({
		parent: {
			padding: ScreenWidth * 0.05,
			flexDirection: 'row',
			borderWidth: 1,
			borderColor: 'black',
			justifyContent: 'space-between'
		},
		left: {
			flexDirection: 'column'
		},
		right: {
			alignSelf: 'center'
		}
	});
	return (
		<View style={styles.parent}>
			<View style={styles.left}>
				<View>
					<Text>{value.createdAt}</Text>
				</View>
				<View>
					<Text>{value.taskName}</Text>
				</View>
			</View>
			<TouchableOpacity onPress={removeTask}>
				<View style={styles.right}>
					<FontAwesome5Icon size={ScreenWidth * 0.07} name={'trash-alt'} />
				</View>
			</TouchableOpacity>
		</View>
	);
};

ListTaskThree.propTypes = {
	value: PropTypes.any,
	removeTask: PropTypes.func
};

export default ListTaskThree;
