import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

const ListMenuCustom = ({ iconLeft, iconRight, textLeft, textRight }) => {
	const styles = StyleSheet.create({
		parent: {
			flexDirection: 'row',
			height: 60,
			backgroundColor: 'white',
			justifyContent: 'space-between',
			paddingHorizontal: 20,
			paddingVertical: 10
		}
	});

	if (iconLeft)
		iconLeft = (
			<View style={{ alignSelf: 'center' }}>
				<FontAwesome5Icon size={26} name={iconLeft} />
			</View>
		);
	if (iconRight)
		iconRight = (
			<View style={{ alignSelf: 'center' }}>
				<FontAwesome5Icon size={26} name={iconRight} />
			</View>
		);
	if (textLeft)
		textLeft = (
			<View style={{ alignSelf: 'center', marginLeft: 20 }}>
				<Text style={{ fontSize: 16 }}>{textLeft}</Text>
			</View>
		);
	if (textRight)
		textRight = (
			<View style={{ alignSelf: 'center', marginLeft: 20 }}>
				<Text style={{ fontSize: 16 }}>{textRight}</Text>
			</View>
		);
		
	return (
		<View style={styles.parent}>
			<View style={{ flexDirection: 'row' }}>
				{iconLeft}
				{textLeft}
			</View>
			<View style={{ flexDirection: 'row' }}>
				{iconRight}
				{textRight}
			</View>
		</View>
	);
};

ListMenuCustom.propTypes = {
	iconLeft: PropTypes.string,
	iconRight: PropTypes.string,
	textLeft: PropTypes.string,
	textRight: PropTypes.string
};

export default ListMenuCustom;
