import React from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

const Gap = ({ mb }) => {
	const styles = StyleSheet.create({
		parent: {
			marginBottom: mb
		}
	});
	return <View style={styles.parent} />;
};

Gap.propTypes = {
	mb: PropTypes.number
};

export default Gap;
