import React from 'react';
import PropTypes from 'prop-types';
import { StatusBar, Text, View } from 'react-native';

const ToolbarCustom = ({ label }) => {
	return (
		<View>
			<StatusBar backgroundColor={'blue'} />
			<View style={{ padding: 20, backgroundColor: 'blue' }}>
				<Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{label}</Text>
			</View>
		</View>
	);
};

ToolbarCustom.propTypes = {
	label: PropTypes.string
};

export default ToolbarCustom;
