import React, { useState } from 'react';
import { Dimensions, Text, TouchableOpacity, View } from 'react-native';
import { Gap, InputCustom, ListTaskThree } from '../../components';

const TodoList = () => {
	let ScreenWidth = Dimensions.get('window').width;
	const [ state, setstate ] = useState({ text: '', task: [] });

	let showlistTask = state.task.map((v) => (
		<View>
			<Gap mb={10} />
			<ListTaskThree key={v.taskName} value={v} removeTask={() => handleDelete(v.taskName)} />
		</View>
	));

	const handleChange = (value) => {
		setstate(() => ({
			...state,
			text: value
		}));
	};

	const handleAdd = () => {
		const now = new Date();

		const data = {
			createdAt: `${now.getDate()}/${now.getMonth()}/${now.getFullYear()}`,
			taskName: state.text
		};

		setstate((currentState) => ({
			...state,
			task: [ ...currentState.task, data ]
		}));
	};

	const handleDelete = (value) => {
		const deleted = state.task.filter((val) => val.taskName !== value);
		setstate(() => ({
			...state,
			task: deleted
		}));
	};

	return (
		<View style={{ padding: ScreenWidth * 0.05 }}>
			<View style={{ flexDirection: 'column' }}>
				<View style={{ width: ScreenWidth }}>
					<Text>Masukkan TodoList</Text>
				</View>
				<Gap mb={10} />
				<View style={{ flexDirection: 'row' }}>
					<View style={{ width: ScreenWidth * 0.675 }}>
						<InputCustom onChangeText={(text) => handleChange(text)} placeholder={'Input Here'} />
					</View>
					<TouchableOpacity onPress={handleAdd}>
						<View
							style={{
								width: ScreenWidth * 0.2,
								paddingVertical: ScreenWidth * 0.0075,
								paddingHorizontal: ScreenWidth * 0.075,
								backgroundColor: 'blue',
								marginLeft: ScreenWidth * 0.025
							}}
						>
							<Text style={{ color: 'white', fontSize: ScreenWidth * 0.1 }}>{'+'}</Text>
						</View>
					</TouchableOpacity>
				</View>
				{showlistTask}
			</View>
		</View>
	);
};

export default TodoList;
