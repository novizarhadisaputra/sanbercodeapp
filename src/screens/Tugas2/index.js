import React from 'react';
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Gap, ListMenuCustom, ToolbarCustom } from '../../components';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { PhotoNovizar } from '../../assets';

const Tugas2 = () => {
	let ScreenHeight = Dimensions.get('window').height;
	let ScreenWidth = Dimensions.get('window').width;
	const styles = StyleSheet.create({
		parent: {
			height: ScreenHeight,
			width: ScreenWidth,
			backgroundColor: '#ebebeb',
			color: 'black'
		},
		logo: {
			borderRadius: 200,
			width: 60,
			height: 60
		},
		icon: {
			width: 30,
			height: 30
		}
	});

	return (
		<View style={styles.parent}>
			<ToolbarCustom label={'Account'} />
			<View style={{ flexDirection: 'column' }}>
				<View
					style={{
						flexDirection: 'row',
						height: 100,
						backgroundColor: 'white',
						paddingHorizontal: 20,
						paddingVertical: 10
					}}
				>
					<View style={{ alignSelf: 'center' }}>
						<Image style={styles.logo} source={PhotoNovizar} />
					</View>
					<View
						style={{
							alignSelf: 'center',
							marginLeft: 20,
							minHeight: 100,
							flex: 1,
							justifyContent: 'center'
						}}
					>
						<Text style={{ fontSize: 18, fontWeight: 'bold' }}>Novizar Hadi Saputra</Text>
					</View>
				</View>
				<Gap mb={1} />
				<TouchableOpacity>
					<ListMenuCustom iconLeft={'wallet'} textLeft={'Saldo'} textRight={'Rp. 120.000.000'} />
				</TouchableOpacity>
				<Gap mb={5} />
				<TouchableOpacity>
					<ListMenuCustom iconLeft={'cogs'} textLeft={'Pengaturan'} />
				</TouchableOpacity>
				<Gap mb={1} />
				<TouchableOpacity>
					<ListMenuCustom iconLeft={'question-circle'} textLeft={'Bantuan'} />
				</TouchableOpacity>
				<Gap mb={1} />
				<TouchableOpacity>
					<ListMenuCustom iconLeft={'file-alt'} textLeft={'Syarat & Ketentuan'} />
				</TouchableOpacity>
				<Gap mb={5} />
				<TouchableOpacity>
					<ListMenuCustom iconLeft={'sign-out-alt'} textLeft={'Keluar'} />
				</TouchableOpacity>
			</View>
		</View>
	);
};

export default Tugas2;
