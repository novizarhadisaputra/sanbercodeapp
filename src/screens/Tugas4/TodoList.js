import React, { useContext } from 'react';
import { Dimensions, Text, StyleSheet, TouchableOpacity, View, FlatList } from 'react-native';
import { RootContext } from '.';
import { Gap, InputCustom } from '../../components';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

const TodoList = () => {
	const { todos, handleChange, handleAdd, handleDelete } = useContext(RootContext);

	const renderItem = ({item}) => {
		return (
			<View style={styles.parent}>
				<View style={styles.left}>
					<View>
						<Text>{item.createdAt}</Text>
					</View>
					<View>
						<Text>{item.taskName}</Text>
					</View>
				</View>
				<TouchableOpacity onPress={() => handleDelete(item.taskName)}>
					<View style={styles.right}>
						<FontAwesome5Icon size={ScreenWidth * 0.07} name={'trash-alt'} />
					</View>
				</TouchableOpacity>
			</View>
		);
	};

	return (
		<View>
			<View style={{ width: ScreenWidth }}>
				<Text>{'Masukkan TodoList'}</Text>
			</View>
			<Gap mb={10} />
			<View style={{ flexDirection: 'row' }}>
				<View style={{ width: ScreenWidth * 0.675 }}>
					<InputCustom onChangeText={handleChange} placeholder={'Input Here'} />
				</View>
				<TouchableOpacity onPress={handleAdd}>
					<View
						style={{
							width: ScreenWidth * 0.2,
							paddingVertical: ScreenWidth * 0.0075,
							paddingHorizontal: ScreenWidth * 0.075,
							backgroundColor: 'blue',
							marginLeft: ScreenWidth * 0.025
						}}
					>
						<Text style={{ color: 'white', fontSize: ScreenWidth * 0.1 }}>{'+'}</Text>
					</View>
				</TouchableOpacity>
			</View>
			<Gap mb={10} />
			<FlatList data={todos} renderItem={renderItem}></FlatList>
		</View>
	);
};

const ScreenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
	parent: {
		padding: ScreenWidth * 0.05,
		flexDirection: 'row',
		borderWidth: 1,
		borderColor: 'black',
		justifyContent: 'space-between',
		marginBottom: 10
	},
	left: {
		flexDirection: 'column'
	},
	right: {
		alignSelf: 'center'
	}
});

export default TodoList;
