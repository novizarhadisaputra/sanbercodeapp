import React, { createContext, useState } from 'react';
import { Dimensions, FlatList, View } from 'react-native';
import { Gap } from '../../components';
import FormInput from './FormInput';
import TodoList from './TodoList';

export const RootContext = createContext();

const { Provider } = RootContext;

const Tugas4 = () => {
	const ScreenWidth = Dimensions.get('window').width;

	const [ input, setInput ] = useState('');
	const [ todos, setTodos ] = useState([]);

	const handleChange = (value) => {
		setInput(value);
	};

	const handleDelete = (value) => {
		const deleted = todos.filter((val) => val.taskName !== value);
		setTodos(deleted);
	};

	const handleAdd = () => {
		const now = new Date();
		const data = {
			createdAt: `${now.getDate()}/${now.getMonth()}/${now.getFullYear()}`,
			taskName: input
		};
		const newTodos = [ ...todos, data ];
		setTodos(newTodos);
		setInput('');
	};

	return (
		<Provider value={{ input, todos, handleChange, handleDelete, handleAdd }}>
			<View style={{ padding: ScreenWidth * 0.05 }}>
				<TodoList />
			</View>
		</Provider>
	);
};

export default Tugas4;
