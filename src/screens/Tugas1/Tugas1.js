import React from 'react';
import { View, Text } from 'react-native';

const Tugas1 = () => {
	return (
		<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
			<Text>Hallo Kelas React Native Lanjutan Sanbercode</Text>
		</View>
	);
};

export default Tugas1;
